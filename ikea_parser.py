# -*- coding: utf-8 -*-
import urllib.request
import os
import re
from urllib.error import HTTPError
from urllib.request import urlretrieve
from bs4 import BeautifulSoup
from progress import printProgress


def list_to_line(product_list, label):
    line = ""
    # sku
    line += product_list[4].strip(u'\t') + ","
    # store view code
    line += ","
    # attribute_set_code
    line += "Default,"
    # product_type
    line += "simple,"
    # categories
    line += "\"Default Category/" + label + "\","
    # product_websites
    line += "base,"
    # name
    line += product_list[0] + ","
    # description
    line += product_list[3].replace(u',',u'') + ","
    # short description
    line += product_list[1].replace(u',',u'') + ","
    # weight
    line += ","
    # product_online
    line += "1,"
    # tax_class_name
    line += "Taxable Goods,"
    # visibility
    line += "\"Catalog, Search\","
    # price
    price = re.search(r'\d+', product_list[2]).group()
    line += price + ","
    # url_key
    line += product_list[4].strip(u'\t') + ","
    # base_image
    line += product_list[5] + ","
    # small_image
    line += product_list[6] + ","
    # thumbnail_image
    line += product_list[7] + ","
    # qty
    line += "100,"
    # out_of_stock_qty
    line += "0,"
    # meta title
    line += product_list[0] + ","
    # meta keywords
    line += product_list[0] + ","
    # meta description
    line += product_list[0] + ","
    # website_id
    line += "1,"
    # additional_attributes
    line += "\"has_options=0,required_options=0\","
    # use_config_min_qty
    line += "1,"
    # is_qty_decimal
    line += "0,"
    # allow_backorders
    line += "0,"
    # use_config_backorders
    line += "1,"
    # min_cart_qty
    line += "1.0000,"
    # use_config_min_sale_qty
    line += "1,"
    # max_cart_qty
    line += "10000.0000,"
    # use_config_max_sale_qty
    line += "1,"
    # is_in_stock
    line += "0,"
    # notify_on_stock_below
    line += "1.0000,"
    # use_config_notify_stock_qty
    line += "1,"
    # manage_stock
    line += "0,"
    # use_config_manage_stock
    line += "0,"
    # use_config_qty_increments
    line += "1,"
    # qty_increments
    line += "1.0000,"
    # use_config_enable_qty_inc
    line += "0,"
    # enable_qty_increments
    line += "0,"
    # is_decimal_divided
    line += "0,"
    # display_product_options_in
    line += "\"Block after Info Column\","
    # created_at
    line += "\"2016-10-22 14:51:08\","
    # updated_at
    line += "\"2016-10-22 14:51:08\"\n"
    return line


def measures_to_list(messy_text):
    messy_text = messy_text.replace(u'\t', u'').replace(u'\r', u'')
    lines = messy_text.split('\n')

    # get rid of \xa0
    for i in range(0, len(lines)):
        lines[i] = lines[i].replace(u'\xa0', u'')

    # get rid off empty lines
    lines = [line for line in lines if line.strip() != '']
    return lines

def item_to_list(messy_text):
    messy_text = messy_text.replace(u'\t', u'').replace(u'\r', u'')
    lines = messy_text.split('\n')

    # get rid of \xa0
    for i in range(0, len(lines)):
        lines[i] = lines[i].replace(u'\xa0', u'')

    # get rid off empty lines
    lines = [line for line in lines if line.strip() != '']

    # remove 'cena za sztukę' and 'dodaj do koszyka' labels
    try:
        lines.remove(u'Cena za sztuke')
        lines.remove(u'Dodaj do listy zakupów')
    except ValueError:
        pass

    lines = [lines[0],lines[1],lines[2]]
    return lines


# list of products, imgs links, and paths to folders
def download_n_save_to_file(product_list, small_img_list, links_list,images_path,small_images_path,thumbnails_path,root_path,items_label):
    print(u'Downloading and saving files for: ' + items_label)

    # check whether there is chairs/images folder
    # note:  it will also create root (path) folder
    if not os.path.exists(root_path + images_path):
        os.makedirs(root_path + images_path)
    if not os.path.exists(root_path + small_images_path):
        os.makedirs(root_path + small_images_path)
    if not os.path.exists(root_path + thumbnails_path):
        os.makedirs(root_path + thumbnails_path)

    # open file to writing a itrem list
    items_list_file = open(root_path + items_label + ".csv", mode="w", encoding="utf-8")

    # add column names to csv
    headings_line = "sku," \
                    "store_view_code," \
                    "attribute_set_code," \
                    "product_type," \
                    "categories," \
                    "product_websites," \
                    "name," \
                    "description," \
                    "short_description," \
                    "weight," \
                    "product_online," \
                    "tax_class_name," \
                    "visibility," \
                    "price," \
                    "url_key," \
                    "base_image," \
                    "small_image," \
                    "thumbnail_image," \
                    "qty," \
                    "out_of_stock_qty," \
                    "meta_title," \
                    "meta_keywords," \
                    "meta_description," \
                    "website_id," \
                    "additional_attributes," \
                    "use_config_min_qty," \
                    "is_qty_decimal," \
                    "allow_backorders," \
                    "use_config_backorders," \
                    "min_cart_qty," \
                    "use_config_min_sale_qty," \
                    "max_cart_qty," \
                    "use_config_max_sale_qty," \
                    "is_in_stock," \
                    "notify_on_stock_below," \
                    "use_config_notify_stock_qty," \
                    "manage_stock," \
                    "use_config_manage_stock," \
                    "use_config_qty_increments," \
                    "qty_increments," \
                    "use_config_enable_qty_inc," \
                    "enable_qty_increments," \
                    "is_decimal_divided," \
                    "display_product_options_in," \
                    "created_at," \
                    "updated_at\n"
    items_list_file.write(headings_line)
    # prepare progress indicator
    i = 0
    printProgress(i, len(product_list), prefix=u'Progress:', suffix=u'Complete', barLength=50)

    # download n save items data
    for small_img, product,link in zip(small_img_list, product_list,links_list):
        # get html doc for item page


        # get html doc
        html_doc = urllib.request.urlopen(link).read().decode("utf-8 ")
        soup = BeautifulSoup(html_doc, 'html.parser')

        # get images links
        img_link = "http://www.ikea.com" + small_img['src'].replace(u'S3', u'S4')
        small_img_link = "http://www.ikea.com" + small_img['src']
        th_img_link = "http://www.ikea.com" + small_img['src'].replace(u'S3', u'S1')

        measures = soup.find("div",attrs={"id":"measuresPart"})

        measures_list = measures_to_list(measures.text)

        # get only measures (if there are any)
        not_found_measure = True
        for measure in measures_list:
            if "Szerokość" in measure:
                not_found_measure = False
                product.append(measure)
                break

        if not_found_measure:
            product.append("brak")

        # get filenames of images
        img_fname = images_path + img_link.split('/')[-1]
        small_img_fname = small_images_path + small_img_link.split('/')[-1]
        th_img_fname = thumbnails_path + th_img_link.split('/')[-1]

        #get product sku
        item_sku = link.split('/')[-2]

        # append fnames and sku to chairs products list
        product.append(item_sku)

        product.append(img_fname)
        product.append(small_img_fname)
        product.append(th_img_fname)

        # write to file
        items_list_file.write(list_to_line(product, items_label))

        # download and save file
        try:
            # image
            urlretrieve(img_link, root_path + img_fname)
            # small image
            urlretrieve(small_img_link, root_path + small_img_fname)
            # thumbnail
            urlretrieve(th_img_link, root_path + th_img_fname)
            # full image

        except FileNotFoundError as err:
            print(err)  # something wrong with local path
        except HTTPError as err:
            print(err)  # something wrong with url

        # show progress
        i += 1
        printProgress(i, len(product_list), prefix=u'Progress:', suffix=u'Complete', barLength=50)
        # i = product_list.index(chair) * 100 / len(product_list)
        # sys.stdout.write("\r%d%%" % i)
        # sys.stdout.flush()

    items_list_file.close()
    print(u"FINISHED!")


# url - url to ikea site (products), items_label -label for those items
def parse(url,items_label):
    ITEMS_DIR = items_label + "/"
    ITEMS_TH_IMG_DIR = "imgs/thumbnails/"
    ITEMS_SM_IMG_DIR = "imgs/images/"
    ITEMS_IMG_DIR = "imgs/small_images/"

    # get html doc
    html_doc = urllib.request.urlopen(url).read().decode("utf-8 ")

    soup = BeautifulSoup(html_doc, 'html.parser')

    # get some info about products
    items_rows = soup.find_all("div", attrs={"class": "threeColumn product "})
    item_rows_2 = soup.find_all("div",attrs={"class":"threeColumn product lastColumn"})
    items_raw = items_rows + item_rows_2
    items_proper = []

    items_small_images = soup.find_all("img", attrs={"class": "prodImg"})
    items_links_raw = soup.find_all("a", attrs={"class": "productLink"})
    items_links = []

    if len(items_raw) != len(items_small_images) and len(items_raw) != len(items_links):
       raise Exception(u'ERROR: Problem that lists for products,images etc. have different sizes. Cannot proceed!')

    # get proper right links
    for link in items_links_raw:
        link = "http://www.ikea.com" + link['href']
        items_links.append(link)

    # get proper items list
    for item in items_raw:
        items_proper.append(item_to_list(item.text))

    # proceed to file save function
    download_n_save_to_file(items_proper,items_small_images,items_links,ITEMS_IMG_DIR,ITEMS_SM_IMG_DIR,ITEMS_TH_IMG_DIR,ITEMS_DIR,items_label)