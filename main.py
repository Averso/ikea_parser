# -*- coding: utf-8 -*-
from ikea_parser import parse

# get links from file
links_file = open("link_list",mode="r",encoding="utf-8")

links_labels_list = links_file.read().splitlines()
for i in range(0, len(links_labels_list)):
    links_labels_list[i] = links_labels_list[i].split('|')

links_file.close()


# for every url
for links_labels_list in links_labels_list:
   # get html doc
   try:
       # url and label
       parse(links_labels_list[0],links_labels_list[1])
   except Exception as err:
       print(err)
