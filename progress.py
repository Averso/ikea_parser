# -*- coding: utf-8 -*-
import sys

# Print iterations progress
def printProgress (iteration, total, prefix = '', suffix = '', decimals = 1, barLength = 100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        barLength   - Optional  : character length of bar (Int)
    """
    formatStr       = u"{0:." + str(decimals) + "f}"
    percents        = formatStr.format(100 * (iteration / float(total)))
    filledLength    = int(round(barLength * iteration / float(total)))
    bar             = u'█' * filledLength + '-' * (barLength - filledLength)
    sys.stdout.write(u'\r%s |%s| %s%s %s' % (prefix, bar, percents, u'%', suffix)),
    if iteration == total:
        sys.stdout.write(u'\n')
    sys.stdout.flush()